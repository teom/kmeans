/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "kmeansalgorithm.h"

#include <QtCore/QTime>

KMeansAlgorithm::KMeansAlgorithm( const DataSet &data,
                                  QVector<int> &clusters,
                                  QVector<QVector<double> > &currentMeans,
                                  int clusterCount,
                                  unsigned int interval,
                                  QObject *parent )
    : AbstractClusteringAlgorithm( data,
                                   clusters,
                                   currentMeans,
                                   clusterCount,
                                   interval,
                                   parent )
{
}

void
KMeansAlgorithm::run()
{
    qDebug() << "Begin kMeans";
    qsrand( QTime::currentTime().msec() );  //whoops, separate thread so we have to seed
    //let's randomly pick a few means...
    for( int j = 0; j < m_clusterCount; ++j )
    {
        int randomRow = qrand() % m_data.points.size();
        qDebug() << "picket at random row " << randomRow;
        //a vector of points
        m_currentMeans.append( QVector< double >( m_data.points.at( randomRow ) ) );
    }

    //let's assign every point to exactly one mean...
    for( int i = 0; i < m_data.points.size(); ++i )
    {
        m_clusters[ i ] = pickMean( m_data.points.at( i ), m_currentMeans );
                /*the cluster-index of the meanpoint that minimizes the distance*/
    }

    bool clustersChanged = true;
    int iterations = 0;
    while( clustersChanged )
    {
        if( m_stopAsap )
            break;

        clustersChanged = false; //let's assume nothing will change in this iteration

        for( int j = 0; j < m_clusterCount; ++j )
        {
            m_currentMeans[ j ] = findCentroid( m_data, m_clusters, j );
        }
        for( int i = 0; i < m_data.points.size(); ++i )
        {
            int newClusterId = pickMean( m_data.points.at( i ), m_currentMeans );

            if( newClusterId != m_clusters[ i ] )
            {
                clustersChanged = true;
                m_clusters[ i ] = newClusterId;
            }
        }
        iterations++;

        emit stepTaken();

        if( m_interval > 0)
            msleep( m_interval );
    }
    qDebug() << "Took " << iterations << " steps!";
}

int
KMeansAlgorithm::pickMean(const QVector<double> &point,
                          const QVector<QVector<double> > &currentMeans) const
{
    int bestMean = 0;
    double bestMeanDistance = std::numeric_limits< double >::infinity();

    for( int k = 0; k < currentMeans.size(); ++k )
    {
        double newDistance = distance( point, currentMeans.at( k ) );
        if( newDistance < bestMeanDistance )
        {
            bestMeanDistance = newDistance;
            bestMean = k;
        }
    }
    return bestMean;
}

QVector< double >
KMeansAlgorithm::findCentroid(const DataSet &data,
                              const QVector<int> &clusters,
                              int clusterId) const
{
    int thisClusterSize = 0;
    QVector< double > centroid;

    centroid.fill( 0.0, data.points.first().size() );

    for( int i = 0; i < data.points.size(); ++i )
    {
        if( clusters.at( i ) != clusterId )
            continue;

        thisClusterSize++;

        const QVector< double > &thisDataPoint = data.points.at( i );

        for( int j = 0; j < thisDataPoint.size(); ++j )
            centroid[j] += thisDataPoint.at( j );
    }

    for( int j = 0; j < centroid.size(); ++j )
        centroid[ j ] /= thisClusterSize;

    return centroid;
}
