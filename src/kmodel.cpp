/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "kmodel.h"
#include "kmeansalgorithm.h"
#include "kmedoidsalgorithm.h"

KModel::KModel( QObject *parent )
    : QObject( parent )
    , m_columnCount( 0 )
    , m_xColumn( 0 )
    , m_yColumn( 0 )
{

}

void
KModel::loadData( const DataSet &data )
{
    m_data = data;
    m_columnCount = m_data.points.first().size();
    m_clusters.clear();
    m_clusterCount = 0;
    m_currentMeans.clear();
    m_xColumn = 0;
    m_yColumn = 1;

    QStringList clustersMap;

    for( int i = 0; i < m_data.points.size(); ++i )
    {
        if( !clustersMap.contains( m_data.labels.at( i ) ) )
            clustersMap << m_data.labels.at( i );
        m_clusters.append( clustersMap.indexOf( m_data.labels.at( i ) ) );
    }

    m_clusterCount = clustersMap.length();

    printOut();
    emit dataChanged();
}

void
KModel::printOut()
{
    int dimCount = m_data.points.first().size();
    QString line;

    for( int j = 0; j < dimCount; ++j )
        line += "Col " + QString::number( j + 1 ) + "\t";

    line += "Clust#\t";
    line += "Label";

    qDebug() << line;

    for( int i = 0; i < m_data.points.size(); ++i )
    {
        line.clear();
        for( int j = 0; j < dimCount; ++j )
            line += QString::number( m_data.points.at( i ).at( j ) ) + "\t";
        line += QString::number( m_clusters.at( i ) ) + "\t";
        line += m_data.labels.at( i );

        qDebug() << line;
    }
}

QPair< double, double >
KModel::data( int row ) const
{
    if( row >= 0 && row < m_data.points.size() )
    {
        return qMakePair( m_data.points.at( row ).at( m_xColumn ),
                          m_data.points.at( row ).at( m_yColumn ) );
    }
    return qMakePair( 0.0, 0.0 );
}

QPair<double, double>
KModel::mean(int clusterId) const
{
    if( clusterId >= 0 && clusterId < m_clusterCount )
    {
        return qMakePair( m_currentMeans.at( clusterId ).at( m_xColumn ),
                          m_currentMeans.at( clusterId ).at( m_yColumn ) );
    }
    return qMakePair( 0.0, 0.0 );
}

int
KModel::cluster(int row) const
{
    return m_clusters.at( row );
}

const QVector<int> &
KModel::clusters() const
{
    return m_clusters;
}

int
KModel::clusterCount() const
{
    return m_clusterCount;
}

int
KModel::meansCount() const
{
    return m_currentMeans.size();
}

int
KModel::columnCount() const
{
    return m_columnCount;
}

void
KModel::setXColumn(int c) //slot
{
    if( c < 0 || c >= m_columnCount )
        return;

    m_xColumn = c;
    emit dataChanged();
}

void
KModel::setYColumn(int c) //slot
{
    if( c < 0 || c >= m_columnCount )
        return;

    m_yColumn = c;
    emit dataChanged();
}

void
KModel::run( int algorithmId, int clusterCount, unsigned int interval )
{
    m_clusterCount = clusterCount;
    m_currentMeans.clear();
    m_clusters.fill( 0 );

    qDebug() << "About to construct algorithm thread.";

    if( m_algorithm )
    {
        if( !m_algorithm->isFinished() )
            m_algorithm->terminate();
        delete m_algorithm;
    }

    if( algorithmId == 0 )
    {
        m_algorithm = new KMeansAlgorithm( m_data,
                                           m_clusters,
                                           m_currentMeans,
                                           m_clusterCount,
                                           interval,
                                           this );
    }
    else if( algorithmId == 1 )
    {
        m_algorithm = new KMedoidsAlgorithm( m_data,
                                           m_clusters,
                                           m_currentMeans,
                                           m_clusterCount,
                                           interval,
                                           this );
    }
    else return;

    connect( m_algorithm, SIGNAL( stepTaken() ), this, SIGNAL( dataChanged() ) );
    connect( m_algorithm, SIGNAL( finished() ), this, SLOT( onAlgorithmDone() ) );

    m_algorithm->start();

    emit crunching();
}

void KModel::stop()
{
    if( m_algorithm )
    {
        if( !m_algorithm->isFinished() )
        {
            m_algorithm->stopAsap();
            m_algorithm->wait( 1000 );
        }
        delete m_algorithm;
    }
    emit crunchingDone();
}

int
KModel::rowCount() const
{
    return m_data.points.size();
}

bool KModel::isCrunching() const
{
    if( m_algorithm )
    {
        if( !m_algorithm->isFinished() )
            return true;
    }
    return false;
}


void KModel::onAlgorithmDone()
{
    delete m_algorithm;
    emit crunchingDone();
}
