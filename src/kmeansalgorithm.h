/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KMEANSALGORITHM_H
#define KMEANSALGORITHM_H

#include "abstractclusteringalgorithm.h"

class KMeansAlgorithm : public AbstractClusteringAlgorithm
{
    Q_OBJECT
public:
    explicit KMeansAlgorithm( const DataSet &data,
                              QVector< int > &clusters,
                              QVector< QVector< double > > &currentMeans,
                              int clusterCount,
                              unsigned int interval,
                              QObject *parent = 0 );

    virtual void run();

    int pickMean( const QVector< double > &point,
                  const QVector< QVector< double > > &currentMeans ) const;

    QVector< double > findCentroid( const DataSet &data,
                                    const QVector< int > &clusters,
                                    int clusterId ) const;

};

#endif // KMEANSALGORITHM_H
