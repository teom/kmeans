/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KMODEL_H
#define KMODEL_H

#include "abstractclusteringalgorithm.h"

#include <qwt/qwt_series_data.h>

#include <QtCore/QStringList>
#include <QtCore/QPointer>

struct DataSet
{
    QVector< QVector< double > > points;
    QStringList labels;
};

class AbstractClusteringAlgorithm;

class KModel : public QObject
{
    Q_OBJECT
public:
    explicit KModel( QObject *parent = 0 );
    virtual ~KModel() {}

    void loadData( const DataSet &data );

    QPair< double, double > data( int row ) const;

    QPair< double, double > mean( int clusterId ) const;

    int cluster( int row ) const;
    const QVector< int > & clusters() const;
    int clusterCount() const;
    int meansCount() const;

    int columnCount() const;

    int rowCount() const;

    bool isCrunching() const;

public slots:
    void setXColumn( int c );
    void setYColumn( int c );

    void run( int algorithmId, int clusterCount, unsigned int interval );
    void stop();

signals:
    void dataChanged();
    void crunching();
    void crunchingDone();

private slots:
    void onAlgorithmDone();

private:
    QPointer< AbstractClusteringAlgorithm > m_algorithm;

    DataSet m_data;
    QVector< QVector< double > > m_currentMeans;
    QVector< int > m_clusters;
    int m_clusterCount;

    void printOut();

    int m_columnCount;

    int m_xColumn;
    int m_yColumn;
};

#endif // KMODEL_H
